import About from "@/components/Home/About/About";
import Banner from "@/components/Home/Banner/Banner";

export default function Page({ pagesData }: Repo) {
  console.log(pagesData);
  return (
    <>
      <Banner heroSection={pagesData.heroSection} />
      <About aboutSection={pagesData.aboutSection}/>
    </>
  );
}
import type { InferGetServerSidePropsType, GetServerSideProps } from "next";

type Repo = {
  pagesData: any;
};

export const getServerSideProps = (async () => {
  // Fetch data from external API
  const res = await fetch("http://localhost:3000");
  const repo: Repo = await res.json();
  // Pass data to the page via props
  return { props: { pagesData: repo.pagesData } };
}) satisfies GetServerSideProps<{ repo: Repo }>;
