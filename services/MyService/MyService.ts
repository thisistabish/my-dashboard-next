import axios from "axios";

class MyService {
  getHomePage() {
    return axios.get("http://localhost:3000");
  }
  getAboutUs() {
    return axios.get("http://localhost:3000/aboutus");
  }
}
export default MyService;