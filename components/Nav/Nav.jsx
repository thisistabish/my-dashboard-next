import React from 'react'
import logo from '../../public/assets/logo.png'
import Image from 'next/image'; // Import Image from next/image


const Nav = () => {
  return (
    <div className='flex m-auto py-[10px] px-[20px] fixed z-[1] bg-white'>
      <Image src={logo} alt='Logo' className='w-[14%]'/>
      <ul className='flex m-auto gap-12 text-lg font-medium	'>
        <li>About us</li>
        <li>Workshop</li>
        <li>Teach with us</li>
      </ul>
    </div>
   
    
  )
}

export default Nav