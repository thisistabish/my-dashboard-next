import React, { useEffect } from "react";
import Button from "../../Button/Button";
import MyService from "@/services/MyService/MyService";

const Banner = ({ heroSection }: { heroSection: any }) => {
  return (
    <div className="bg-[url('http://skillnation.in/wp-content/uploads/2023/08/nick-morrison-FHnnjk1Yj7Y-unsplash.jpg')] w-[100%] h-[100vh] bg-cover bg-center  pt-[85px] ">
      <div className="bg-[#071626] opacity-65 h-[100vh] w-full absolute top-0 left-0"></div>
      <div className="text-white text-center relative py-[45px]">
        <h1 className="text-[75px] font-semibold leading-none		">
          {heroSection.heading}
        </h1>
        <p className="text-xl pt-[20px] pb-[40px]	">
          Learn modern age skills directly from Industry Experts and advance
          your career with certified workshops.
        </p>
        <Button
          text="Start Learning"
          className="px-[58px] text-black text-3xl py-[16px] rounded-md bg-[#fdce0e] font-medium"
        />
      </div>
    </div>
  );
};

export default Banner;
