import React from "react";
import Image from "next/image";
import Icon from "../../../public/assets/icon-svg.jpg";
import Button from "../../Button/Button";

const Learning = () => {
  return (
    <div className="bg-[#edf3f8]">
      <div className="bg-[#edf3f8] max-w-screen-lg m-auto">
        <div className="text-center">
          <h2 className="text-4xl font-bold">
            Making your learning convenient & affordable!
          </h2>
          <p className="text-lg mt-[15px] leading-snug">
            Our workshops are designed to be extremely convenient for students &
            working professionals to learn & manage work together. Offered at
            less than the cost of a pizza, upskilling has never been so
            affordable.
          </p>
          <div className="grid grid-rows-1 grid-cols-3	">
            <div className="gap-[15px] items-center	flex flex-col">
              <div className="bg-white rounded-full w-[30%] py-[16px] m-auto ">
                <Image src={Icon} alt="Icon" className="w-[68px] m-auto " />
              </div>
              <Button
                text="Step 1"
                className=" text-white w-[55%] text-[20px] rounded-[4px] bg-[#002950] font-[500] py-[2px]"
              />
              <h5 className="font">Convenient Video Lessons</h5>
              <span>
                Get access to premium video lessons created by Industry Experts
                to learn at your own convenience.
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Learning;
