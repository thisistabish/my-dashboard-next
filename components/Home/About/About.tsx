import React from "react";

const About = ({
  aboutSection,
}: {
  aboutSection: {
    heading: string;
    descripton: string;
    features: {
      number: string;
      text: string;
    }[];
  };
}) => {
  return (
    <div className="max-w-screen-lg m-auto flex gap-[3%] ">
      <div className="w-[50%] pt-[28px]">
        <h2 className="text-4xl font-bold">{aboutSection.heading}</h2>
        <p className="text-lg mt-[15px] leading-snug	">
          {aboutSection.features[0].number}
          As per the Labour Bureau of India, our country has the highest number
          of unemployed educated youth - a staggering 14% of all graduates are
          unemployed.
          <br />
          <br />
          We identified that the driving factor behind the worrisome number was
          the gap between skill & formal education. With corporates continuing
          to value skills over degree & book knowledge, our education system
          fell behind in providing adequate opportunities to our youth.
        </p>
        <p className="italic underline pt-2 text-base	">Read More</p>
      </div>
      <div className="w-[50%] ">
        <div className="flex items-center	 gap-[4%] mt-[28px] ">
          <div className="bg-[#051a2d]  text-white py-[35px] text-center w-[48%] rounded-3xl ">
            <h2 className="text-5xl font-semibold	">200K</h2>
            <p>Learners Upskilled</p>
          </div>
          <div className="bg-[#051a2d]  text-white py-[35px] text-center w-[48%] rounded-3xl ">
            <h2 className="text-5xl font-semibold	">20+</h2>
            <p>Expert Mentors</p>
          </div>
        </div>
        <div className="flex align-middle items-center gap-[4%] mt-[14px] ">
          <div className="bg-[#051a2d]  text-white py-[35px] text-center w-[48%] rounded-3xl ">
            <h2 className="text-5xl font-semibold	">450+</h2>
            <p>Batches Conducted</p>
          </div>
          <div className="bg-[#051a2d]  text-white py-[35px] text-center w-[48%] rounded-3xl ">
            <h2 className="text-5xl font-semibold	">20+</h2>
            <p>Workshops Offered</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
